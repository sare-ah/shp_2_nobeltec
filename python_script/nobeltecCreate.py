# Import modules
import arcpy, os, sys, string
import arcpy
from arcpy import env

# Create envrionmental variables
arcpy.env.overwriteOutput = True
arcpy.env.workspace = arcpy.GetParameterAsText(4)

# Set variables from parameters from user
inputFC=arcpy.GetParameterAsText(0)

# Create the text file to write the field values to
inputfolder=arcpy.GetParameterAsText(4) + "\Export4Nobeltec.txt"
navpoints=open(inputfolder ,"w")

inputfolder2=arcpy.GetParameterAsText(4) + "\Export4Lowrance.csv"
navpoints2=open(inputfolder2 ,"w")

# Build navigation marks (Nobeltec)
rows = arcpy.SearchCursor(inputFC) 
for row in rows: 
          label=row.getValue(arcpy.GetParameterAsText(1))
          navpoints.write('[' + label + ']\n')  
          navpoints.write('Type = Mark\n')   
          lon = row.getValue(arcpy.GetParameterAsText(3))  
          lonDD = int(abs(lon))  
          lonMM = float((float(abs(lon)) - lonDD) * 60.0)  
          lonMMs = str(lonMM)
          lat = row.getValue(arcpy.GetParameterAsText(2))  
          latDD = int(lat)
          latMM = float((float(lat) - latDD) * 60.0)
          latMMs = str(latMM)
     
          navpoints.write('LatLon = ' + str(latDD) + ' ' + latMMs + ' N ' + str(lonDD) + ' ' + lonMMs + ' W\n')  
             
          #navpoints.write('Color = 0x00ff00\n')
          navpoints.write('Color = 0xff0000\n')
          navpoints.write('BmpIdx = 17\n')
          navpoints.write('ExistsOutsideCollection = True\n')
          navpoints.write('Locked = TRUE\n')  
          navpoints.write('MarkRenamed = True\n') 
navpoints.close()

# Build navigation marks (Lowrance)
rows2 = arcpy.SearchCursor(inputFC)
for row in rows2: 
          label=row.getValue(arcpy.GetParameterAsText(1))
          lon = row.getValue(arcpy.GetParameterAsText(3))
          lonstr=str(lon)
          lat = row.getValue(arcpy.GetParameterAsText(2))
          latstr=str(lat)
          navpoints2.write('' + label +',' + lonstr + ',' + latstr + ',' + '\n')             
navpoints2.close()

arcpy.AddMessage("done!")
