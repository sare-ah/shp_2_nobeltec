# shp_2_nobeltec

__Main author:__  Sarah Davies    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Biological Station   
__Contact:__      e-mail:Sarah.Davies@dfo-mpo.gc.ca  | tel:205.756.7124

Detailed instructions on how to use Pts2Nobeltec.tbx can be found in How_to_use_toolbox.docx

sample_data folder contains a sample points dataset (example_pts.shp)